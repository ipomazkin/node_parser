import { getContent } from "./helpers/puppeteer";
import cherio from 'cherio';
import chalk from 'chalk';
import { parser } from "./classes/parser";

const SITE = 'https://exist.ru';
const CATALOG_URI = '/Catalog/Goods/7/3';

try {
    const p = new parser({
        domain: SITE,
        uri: CATALOG_URI
    });

    p.scanLinks();

}
catch (e) {
    console.log(chalk.red('An error has occured: ' + e));
}
