import puppeteer from 'puppeteer';
import select from 'puppeteer-select';
import chalk from 'chalk';

export const LAUNCH_PUPPETEER_OPTS = {
    args: [
        '--window-size:1920x1080',
        '--full-memory-crash-report'
    ],
    headless: true
};


export async function getContent(url) {
    let browser;
    try
    {
        browser  =  await puppeteer.launch(LAUNCH_PUPPETEER_OPTS);
        const page = await browser.newPage();
        await page.goto(url);
        const content  = await page.content();
        await browser.close();
        return content;
    }
    catch (e) {
        console.log(chalk.red(e));
        console.log(chalk.red('closing browser..'));
        if (!!browser)
        {
            await browser.close();
        }
        console.log(chalk.white('OK'));
        return null;
    }

}

/**
 *
 * @param url
 * @param selector
 * @returns {Promise<*>}
 *
 * Получить контент старницы после клика на
 * элемент selector
 */
export async function getContentAfterClick(url, selector) {
    setTimeout( ()=> {},200);
    return 'done';
    // const browser =  await puppeteer.launch(LAUNCH_PUPPETEER_OPTS);
    // const page = await browser.newPage();
    // await page.goto(url);
    // const element = await select(page).getElement(selector);
    // await element.click();
    // const content  = await page.content();
    // await browser.close();
    // return content;
}

