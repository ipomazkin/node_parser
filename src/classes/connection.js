import mysql from 'mysql';

export const CONNECTION_PROPS = {
    host     : '149.154.64.192',
    user     : 'remote',
    password : 'pLOGKzZzJJw8I3zG',
    database : 'remote'
};

class Connection {


    /**
     *
     * @returns {Promise<*>}
     */
    async createConnection() {
        this.db = await mysql.createConnection(CONNECTION_PROPS);
        await this.db.connect();
    }



    /**
     *
     * @param q
     * @param values array
     * @param callback
     * @returns {Promise<void>}
     */
    async query(q,values, callback = ()=>{}) {
        return await this.db.query({
            sql: q,
            timeout: 10000,
            values: values
        },  async (error, results, fields) => {
                      await callback(results);
        });
    }

}

export const connection = new Connection();
