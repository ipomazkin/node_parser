import { getContent, getContentAfterClick } from "../helpers/puppeteer";
import chalk from "chalk";
import cherio from 'cherio';
import { connection } from './connection';
import download from 'image-downloader';

const OIL_TABLE='oil';

export class parser {

    constructor(props) {
        const { domain, uri } = props;
        this.url = domain + uri;
        this.domain = domain;
        this.imgFolder = 'c:/ospanel/domains/node_parser/public/images';
    }

    async getLinks() {
        await connection.createConnection();
        console.log(chalk.green('scanning ' + this.url));
        this.getLastLink(this.url).
        then(  async (lastLink)=> {
            let paginationContent;
            await connection.query('truncate table oil');
            await connection.query('SET GLOBAL wait_timeout = ?',[600]);
            console.log(chalk.green('total pages: ' + lastLink));
            for (let i=1;i<=parseInt(lastLink);i++) {
                paginationContent = await this.mapOnPagination(i).then(async (content)=> {
                    if (content === null)
                        return;
                    const $ = cherio.load(content);
                    $('.cell2').each(async (i, item)=> {
                        const link  = $(item).find('a').attr('href');
                        const queryResult = await this.storeLink(link);
                        console.log(queryResult);
                    });
                });
            }

        });
    }

    async downloadIMG(options) {
        try {
            const { filename, image } = await download.image(options);
            console.log(filename) // => /path/to/dest/image.jpg
        } catch (e) {
            console.error(e)
        }
    }

    /**
     *
     * @param url
     * @returns {Promise<void>}
     *
     * Получаем последнюю сылку
     */
    async getLastLink(url){
        const content = await getContent(url);
        const $ = cherio.load(content);
        return $('#ctl00_ctl00_b_b_ucPagerHead').children('a').last().text();
    }

    /**
     *
     * @returns {Promise<void>}
     * получаем контент по каждой сслыке
     */
    async mapOnPagination(page) {
        const url = this.url + '?Page=' + page;
        console.log(chalk.green(url));
        return await getContent(url);
    }


    clear(str){
        return str.replace(/\W/g,'');
    }


    async scanLinks() {
        await connection.createConnection();
        await connection.query('select id, url from oil where is_parsed=0 order by id asc',null, async (result)=>{

            for (let i=0; i<result.length; i++)  {
                const {id, url} = result[i];
                const link = this.domain + url;
                console.log(chalk.green(`Scan link  ${link}... `));
                try
                {
                    const content = await getContent(link);
                    const $ = cherio.load(content);
                    const application = ($('span[itemprop="description"]').text());
                    const oil_content = $('.ZeForm').html();
                    const article = this.clear($('span:contains(\'Артикул\')').parent().next().text());
                    const brand = ($('span:contains(\'Бренд\')').parent().next().text());
                    const title = ($('span:contains(\'Наименование\')').parent().next().text());
                    const weight = ($('span:contains(\'Вес, г\')').parent().next().text());
                    const width = ($('span:contains(\'Ширина упаковки, мм\')').parent().next().text());
                    const height = ($('span:contains(\'Толщина упаковки, мм\')').parent().next().text());
                    const length = ($('span:contains(\'Длина упаковки, мм\')').parent().next().text());
                    const cont = ($('span:contains(\'Тип контейнера\')').parent().next().text());
                    const oem = ($('span:contains(\'Спецификации OEM\')').parent().next().text());
                    const api = ($('span:contains(\'Спецификации API\')').parent().next().text());
                    const acea = ($('span:contains(\'Спецификации ACEA\')').parent().next().text());
                    const structure = ($('span:contains(\'Состав\')').parent().next().text());
                    const volume = ($('span:contains(\'Объём, л\')').parent().next().text());
                    const viscosity = ($('span:contains(\'Вязкость\')').parent().next().text());
                    const img_src = $('#ctl00_ctl00_b_b_ctl00_imgMain').attr('src');
                    const params = [article,brand, application,title, weight,width,height,length,cont, oem, api, acea, structure, volume, viscosity,img_src, id];
                    await this.addData(params);
                    const options = {
                        url: img_src.replace('//','http://'),
                        dest: this.imgFolder + '/' + id + '.jpg'
                    };
                    await this.downloadIMG(options);
                }
                catch (e) {
                    console.log(chalk.red(e));
                }

            }

        });
    }

    async addData(params) {
        if (!!connection.conn)
            await connection.createConnection();
        const query = `update ${OIL_TABLE} set 
                        article=?,
                        brand=?,
                        application=?,
                        title=?,
                        weight = ?,
                        width = ?,
                        height = ?,
                        length = ?,
                        cont = ?,
                        oem = ?,
                        api = ?,
                        acea = ?,
                        structure =?,
                        volume = ?,
                        viscosity = ?,
                        img_src= ?,
                        is_parsed=1
                        where id= ? `;
        try
        {
            const result = await connection.query(query, Array( ...params ));
            return result;
        }
        catch (e) {
            console.log(chalk.red(e));
            return null;
        }
    }

    /**
     *
     * @param link
     * @returns {Promise<void|PermissionStatus>}
     */
    async storeLink(link) {
        if (!!connection.conn)
            await connection.createConnection();

        const query = `insert ignore into ${OIL_TABLE} (url) values (?)`;
        try
        {
            const result = await connection.query(query, Array(link));
            return result;
        }
        catch (e) {
            console.log(chalk.red(e));
            return null;
        }

    }


}
